/** @odoo-module **/

import { registry } from "@web/core/registry";
import { useService } from "@web/core/utils/hooks";
import { CharField } from "@web/views/fields/char/char_field";


export class CharFieldVoice extends CharField {
    setup() {
        super.setup();
        this.notification = useService('notification');
    }

    async onClickButtonCharMicrophone (ev) {
        const self = this;
        this.stop_recognition = false;
        var $this = $(ev.currentTarget);
        const charInput = document.querySelector(".o_input"); // Char Input
        // The speech recognition interface lives on the browser’s window object
        const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition; // if none exists -> undefined
        console.log("SpeechRecognition", SpeechRecognition);

        if(SpeechRecognition) {
            if (this.notification) {
                this.notification.add(
                    this.env._t("Your Browser supports speech Recognition"),
                    { type: 'info' },
                );
            }
            const recognition = new SpeechRecognition();
            recognition.continuous = true;
            const micBtn = document.querySelector(".o_Char_buttonMicrophone");
            const micIcon = micBtn.firstElementChild;
            micIcon.classList.remove("fa-microphone");
            micIcon.classList.add("fa-microphone-slash");
            recognition.start(); // First time you have to allow access to mic!
            function startSpeechRecognition() {
                charInput.focus();
                this.stop_recognition = false;
                this.notification.add(
                    this.env._t("Start Recording"),
                    { type: 'info' },
                );
            }
            function endSpeechRecognition() {
                micIcon.classList.remove("fa-microphone-slash");
                micIcon.classList.add("fa-microphone");
                charInput.focus();
                recognition.stop();
                this.stop_recognition = true;
                if (this.notification) {
                    this.notification.add(
                        this.env._t("Speech recognition service disconnected"),
                        { type: 'danger' },
                    );
                }
            }
            function resultOfSpeechRecognition(event) {
                const current = event.resultIndex;
                const transcript = event.results[current][0].transcript;
                if (transcript.toLowerCase().trim() === "stop" || this.stop_recognition === true) {
                    recognition.stop();
                } else if (!this.props.value) {
                    this.props.update(transcript);
                } else {
                    if (transcript.toLowerCase().trim() === "go") {
                        // charInput.click();
                        console.log("Go");
                    } else if (transcript.toLowerCase().trim() === "reset") {
                        this.props.update("");
                        console.log("Reset");
                    } else {
                        this.props.update(transcript);
                        console.log("Last");
                    }
                }
            }
            recognition.addEventListener("start", startSpeechRecognition.bind(this));
            recognition.addEventListener("end", endSpeechRecognition.bind(this));
            recognition.addEventListener("result", resultOfSpeechRecognition.bind(this));
            charInput.addEventListener('blur', endSpeechRecognition, false);
          }
        else {
            this.notification.add(
                this.env._t("Your Browser does not support Speech Recognition"),
                { type: 'danger' },
            );
        }
    }
}
CharFieldVoice.template = "widget_voice_to_text_char.CharFieldVoice";
registry.category("fields").add("char_voice", CharFieldVoice);
