{
    "name": "Widget Field Char - Voice to Text for Fields Char",
    "version": "16.0.0.0.0",
    "summary": "Widget Field Char",
    "license": "LGPL-3",
    "author": "Franyer Hidalgo VE <fhidalgo.dev@gmail.com>",
    "category": "Technical",
    "contributors": [
        "Franyer Hidalgo VE <fhidalgo.dev@gmail.com>",
    ],
    "website": "https://gitlab.com/fhidalgo.dev",
    "depends": ["web"],
    "data": [
    ],
    "demo": [],
    "assets": {
        'web.assets_backend': [
            'widget_voice_to_text_char/static/src/components/*/*.xml',
            'widget_voice_to_text_char/static/src/js/char_field_voice.js',
        ],
    },
    "images": [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
