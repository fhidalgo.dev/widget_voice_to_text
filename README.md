
<!-- /!\ do not modify above this line -->

# Speech to text widgets

This repository is intended to place all those modules that in some way or another have some integration of speech to text

<!-- /!\ do not modify below this line -->

<!-- prettier-ignore-start -->

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[widget_voice_to_text_char](widget_voice_to_text_char/) | 16.0.0.0.0 | [![fhidalgo.dev](https://github.com/fhidalgodev.png?size=30px)](https://github.com/fhidalgodev) | Widget Field Char - Voice to Text for Fields Char

[//]: # (end addons)

<!-- prettier-ignore-end -->

## Licenses

This repository is licensed under [LGPL-3.0](LICENSE).
